#!/bin/bash
set -e

SERVER="greenery_dev_pg";

. .env

echo "echo stop & remove old docker [$SERVER] and starting new fresh instance of [$SERVER]"
(docker kill $SERVER || :) && \
  (docker rm $SERVER || :) && \
  docker run \
    --name $SERVER \
    -e POSTGRES_PASSWORD=$DB_PASS \
    -e PGPASSWORD=$DB_PASS \
    -p $DB_PORT:$DB_PORT \
    -d postgres

# wait for pg to start
echo "sleep wait for pg-server [$SERVER] to start";
SLEEP 3;

# create the db
echo "CREATE DATABASE $DB_NAME ENCODING 'UTF-8';" | docker exec -i $SERVER psql -U postgres
echo "\l" | docker exec -i $SERVER psql -U postgres

echo "Postgres running at localhost:$DB_PORT"
