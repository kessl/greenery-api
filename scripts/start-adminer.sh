#!/bin/bash
set -e

SERVER="greenery_dev_adminer";

echo "echo stop & remove old docker [$SERVER] and starting new fresh instance of [$SERVER]"
(docker kill $SERVER || :) && \
  (docker rm $SERVER || :) && \
  docker run \
    --name $SERVER \
    -e ADMINER_DEFAULT_SERVER=host.docker.internal \
    -p 8080:8080 \
    -d adminer

echo "Adminer running at localhost:8080"
