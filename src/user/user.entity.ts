import { Entity, Column } from 'typeorm'
import { BaseEntity } from '../model/base.entity'

@Entity({ name: 'user' })
export class User extends BaseEntity {
  @Column({ type: 'varchar', length: 300 })
  name: string
}
