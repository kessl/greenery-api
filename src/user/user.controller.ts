import { Body, Controller, Get, NotFoundException, Param, Post } from '@nestjs/common'
import { UserService } from './user.service'

@Controller('users')
export class UserController {
  constructor(private readonly userService: UserService) {}

  @Get('/:id')
  async getUser(@Param('id') id: string): Promise<string> {
    const user = await this.userService.findOne(id)

    if (!user) {
      throw new NotFoundException(`user with id '${id}' not found`)
    }

    return user.name
  }

  @Post()
  async insertUser(@Body('name') name: string) {
    return this.userService.insert(name)
  }
}
