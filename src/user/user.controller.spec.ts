import { BadRequestException, NotFoundException } from '@nestjs/common'
import { Test } from '@nestjs/testing'
import { getRepositoryToken } from '@nestjs/typeorm'
import { UserController } from './user.controller'
import { User } from './user.entity'
import { UserService } from './user.service'

describe('UserController', () => {
  let userController: UserController
  let userService: UserService
  let findOne: jest.Mock
  let insert: jest.Mock

  beforeEach(async () => {
    findOne = jest.fn()
    insert = jest.fn()

    const user = await Test.createTestingModule({
      providers: [
        UserService,
        {
          provide: getRepositoryToken(User),
          useValue: { findOne, insert },
        },
      ],
      controllers: [UserController],
    }).compile()

    userService = user.get<UserService>(UserService)
    userController = user.get<UserController>(UserController)
  })

  describe('getUser', () => {
    it('should throw on invalid input', async () => {
      findOne.mockImplementation(() => {
        throw new Error()
      })
      await expect(userController.getUser('invalid-uuid')).rejects.toThrow()
    })

    it('should return 404 if not found', async () => {
      findOne.mockReturnValue(Promise.resolve(undefined))
      await expect(userController.getUser('35c20c76-6b13-4074-b3c5-38c90c47a3ea')).rejects.toThrow(
        new NotFoundException(`user with id '35c20c76-6b13-4074-b3c5-38c90c47a3ea' not found`),
      )
    })

    it('should return user name', async () => {
      const name = 'user-name'
      findOne.mockReturnValue(Promise.resolve({ name }))
      const res = await userController.getUser('35c20c76-6b13-4074-b3c5-38c90c47a3ea')
      expect(res).toEqual(name)
    })
  })

  describe('insertUser', () => {
    it('should return id of inserted entity', async () => {
      const mockResult = {
        id: '35c20c76-6b13-4074-b3c5-38c90c47a3ea',
      }
      insert.mockReturnValue({ identifiers: [mockResult] })

      const res = await userController.insertUser('test-name')
      expect(res).toEqual(mockResult)
    })
  })
})
