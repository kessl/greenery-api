import { Injectable } from '@nestjs/common'
import { InjectRepository } from '@nestjs/typeorm'
import { User } from './user.entity'
import { Repository } from 'typeorm'

@Injectable()
export class UserService {
  constructor(
    @InjectRepository(User)
    private usersRepository: Repository<User>,
  ) {}

  findOne(id: string) {
    return this.usersRepository.findOne(id)
  }

  async insert(name: string) {
    const result = await this.usersRepository.insert({
      name,
      createdBy: 'anon',
      lastChangedBy: 'anon',
    })

    return result.identifiers[0]
  }
}
