import { ConfigService } from '@nestjs/config'

export const getTypeOrmConfig = async (configService: ConfigService) => ({
  type: 'postgres' as 'postgres',
  host: configService.get<string>('DB_HOST', 'localhost'),
  port: configService.get<number>('DB_PORT', 5432),
  username: configService.get<string>('DB_USER', 'postgres'),
  password: configService.get<string>('DB_PASS', 'postgres'),
  database: configService.get<string>('DB_NAME', 'dev'),
  entities: ['dist/**/*.entity{.ts,.js}'],
  synchronize: true,
})
